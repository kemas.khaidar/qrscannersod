from django.contrib import admin
from django.views.generic.base import RedirectView
from django.urls import include, path, re_path
import qr_admin.urls as qr_admin

urlpatterns = [
    re_path(r'^$', RedirectView.as_view(url='qr_admin/', permanent = True), name='$'),
    path('admin/', admin.site.urls),
    path('qr_admin/', include('qr_admin.urls')),
]
