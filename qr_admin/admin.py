from django.contrib import admin
from .models import Order, Ticket, ExcelFileUpload

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass

@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    pass

@admin.register(ExcelFileUpload)
class ExcelFileUploadAdmin(admin.ModelAdmin):
    pass