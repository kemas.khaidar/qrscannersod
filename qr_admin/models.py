from django.db import models

class Order(models.Model):
    order_id = models.CharField(max_length=10, primary_key=True)
    ticket_count = models.IntegerField()
    name = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=20, blank=True)

class Ticket(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    ticket_id = models.CharField(max_length=13, primary_key=True)
    redeemed = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)

class ExcelFileUpload(models.Model):
    excel_file = models.FileField(upload_to='static/excel')