from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from .models import Ticket, ExcelFileUpload
from .utils import generate_qr, parse_excel

import logging

response = {}

@login_required
def tables(request):
    tickets = Ticket.objects.all()
    response['tickets'] = tickets
    return render(request, 'dashboard.html', response)

@login_required
def summary(request):
    ticket_count = Ticket.objects.all().count()
    redeemed_ticket = Ticket.objects.filter(redeemed=True).count()
    response['ticket_count'] = ticket_count
    response['redeemed_ticket'] = redeemed_ticket
    return render(request, 'summary.html', response)

@login_required
def qr_code_reader(request):
    return render(request, 'qrcodereader.html', response)

@login_required
def upload_data(request):
    return render(request, 'uploadData.html', response)

@login_required
def upload_excel(request):
    try:
        if request.method == 'POST' and request.FILES['myFile']:
            parse_excel(request.FILES['myFile'])
    except Exception:
        logging.exception('An exception was thrown!')
    return render(request, 'uploadData.html', response)

@login_required
@csrf_exempt
def validate(request):
    ticket_id = request.POST.get("ticketId")
    exists = Ticket.objects.filter(ticket_id__iexact=ticket_id).exists()
    data = {
        'exists': exists
    }
    return JsonResponse(data)

@login_required
@csrf_exempt
def flag(request):
    if request.method == 'POST':
        ticket_id = request.POST.get('ticketId')
        ticket = Ticket.objects.get(ticket_id=ticket_id)
        data = {
            'redeemed': ticket.redeemed
        }
        if ticket.redeemed:
            return JsonResponse(data)
        ticket.redeemed = True
        ticket.save()
        return JsonResponse(data)

def generate_qr_image(request):
    generate_qr('1201710728002', f'{settings.BASE_DIR}/static/ticket.jpg')
    return JsonResponse({})
    