from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'qr_admin'

urlpatterns = [
    re_path(r'^$', auth_views.LoginView.as_view(template_name= 'login.html'), name='login'),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(next_page= '/qr_admin'), name='logout'),
    re_path(r'^tables/', views.tables, name = 'tables'),
    re_path(r'^summary/', views.summary, name = 'summary'),
    re_path(r'^qrCodeReader/', views.qr_code_reader, name = 'qrCodeReader'),
    re_path(r'^uploadData/', views.upload_data, name = 'uploadData'),
    re_path(r'^uploadExcel/', views.upload_excel, name = 'uploadExcel'),
    re_path(r'^validate/', views.validate, name = 'validate'),
    re_path(r'^flag/', views.flag, name = 'flag'),
    re_path(r'^generateQR/', views.generate_qr_image, name = 'generateQR'),
]