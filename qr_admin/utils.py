from django.conf import settings
from PIL import Image

from .models import Order, Ticket, ExcelFileUpload

import pandas as pd
import qrcode

def parse_excel(file):
    print('uploading')
    upload_obj = ExcelFileUpload.objects.create(excel_file=file)
    df = pd.read_excel(f'{settings.BASE_DIR}/{upload_obj.excel_file}')
    for index, row in df.iterrows():
        order_id = row['Order ID']
        tickets = row['Ticket ID(s)'].split(',')
        ticket_count = int(row['Tickets'])
        name = row['Name']
        email = row['Email']
        phone = row['Phone']
        new_order = Order.objects.create(
            order_id = order_id,
            ticket_count = ticket_count,
            name = name,
            email = email,
            phone = phone
        )
        new_order.save()
        for ticket in tickets:
            new_ticket = Ticket.objects.create(order=new_order, ticket_id=ticket.split(' ')[0])
            new_ticket.save()
 
def generate_qr(ticket_id, logo_link):
    logo = Image.open(logo_link)
    basewidth = 200
    qr_code = qrcode.QRCode(
        error_correction=qrcode.constants.ERROR_CORRECT_H
    )
    qr_code.add_data(ticket_id)
    qr_code.make()
    qr_color = 'Black'
    qr_img = qr_code.make_image(
    fill_color=qr_color, back_color="white").convert('RGB')
    pos = ((logo.size[0] - qr_img.size[0]) // 8, 25)
    wpercent = (basewidth/float(qr_img.size[0]))
    hsize = int((float(qr_img.size[1])*float(wpercent)))
    qr_img = qr_img.resize((basewidth, hsize), Image.ANTIALIAS)
    logo.paste(qr_img, pos)
    logo.save('TEST.png')
