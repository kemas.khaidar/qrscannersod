from django.apps import AppConfig


class QrAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'qr_admin'
